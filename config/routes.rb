Rails.application.routes.draw do

  devise_for :users, :controllers => {:registrations => "registrations"}
	root 'app#index'

	resources :app, only: [:index]

	resources :users, only: [] do
		collection do
			get :papers
		end
	end
	
	resources :events do
		member do
			get :evaluators
			get :papers
			get :share
			post :apply_evaluator
		end
	end

	resources :papers do
		member do
			get :apply_evaluator
			get :ratings
			get :evaluate
			post :apply_evaluation
		end
	end

	resources :event_users, only: [] do 
		member do
			get :accept
			get :refuse
		end
	end
end
