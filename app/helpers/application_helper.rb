module ApplicationHelper

	def get_status_string status
		case status
		when "refused"
			"Recusado"
		when "accepted"
			"Aceito"
		else
			"Pendente"
		end
	end

	def format_date date
		date.nil? ? "" : date.utc.strftime("%d/%m/%Y")
	end

end
