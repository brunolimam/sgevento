class EventUsersController < ApplicationController
	before_action :set_event_user, only: [:accept, :refuse]

	def accept
		@event_user.accepted!

		redirect_back(fallback_location: root_path)
	end

	def refuse
		@event_user.refused!

		redirect_back(fallback_location: root_path)
	end

	private
	def set_event_user
		@event_user = EventUser.find_by_id(params[:id])
	end
	
	
end