class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    user = User.create(user_params)
    if user.valid?
      redirect_to "/"
    else
      redirect_to new_user_registration_path
    end
  end

  def update
    super
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :user_type_id)
  end
end 