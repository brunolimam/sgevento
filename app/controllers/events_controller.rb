class EventsController < ApplicationController
	
	before_action :set_event, only: [:edit, :destroy, :update, :evaluators, :papers, :share, :apply_evaluator]
	before_action :check_token, only: [:share]

	def index
		@events = current_user.events.paginate(:page => params[:page], :per_page => 10)

		@events = @events.includes(:token).order(start_at: :desc)
	end

	def new
		@event = Event.new
	end

	def create
		@event = Event.new(event_params)
		@event.bind_dates(params[:event][:start_at], params[:event][:end_at], params[:event][:accept_papers_until])
		@event.user = current_user

		criteria = create_criteria

		@event.save
		@event.criteria << criteria


		redirect_to events_path
	end

	def edit
	end

	def update
		@event.update_attributes(event_params)
		@event.criteria << update_criteria

		redirect_to events_path
	end

	def destroy
		@event.destroy

		redirect_to events_path
	end

	def evaluators
		@teachers_event_users = @event.teachers_event_users.includes(:user).order("users.name")
	end

	def papers
		@papers = @event.papers.includes(:students).order(title: :asc)
	end

	def share
		@paper = Paper.new(event: @event)

	end

	def apply_evaluator
		if !current_user.user_type.teacher? || @event.teachers.exists?(current_user.id)
			redirect_to "/"
			return
		end

		@event.teachers << current_user
		redirect_to "/"
	end

	private
	def set_event
		@event = Event.find_by_id(params[:id])
	end

  def event_params
    params.require(:event).permit(:title, :place, :description, :start_at, :end_at, :accept_papers_until, :user)
  end

  def check_token
  	@valid_token = @event.valid_token?(params[:token]) 
  end


  def create_criteria
  	return [] if params[:criteria].nil?

  	criteria = []

  	params[:criteria].each do |criterium_param|
  		next if criterium_param[:remove]
  		criteria << Criterium.create(name: criterium_param[:name])
  	end

  	criteria
  end

  def update_criteria
  	return [] if params[:criteria].nil?

		newest_criteria = []
  	params[:criteria].each do |criterium_param|

  		if !criterium_param[:id].blank? && criterium_param[:remove]
  			Criterium.find(criterium_param[:id]).destroy
  		elsif !criterium_param[:id].blank?
  			Criterium.find(criterium_param[:id]).update_attributes(name: criterium_param[:name])
  		elsif !criterium_param[:remove]
				newest_criteria << Criterium.create(name: criterium_param[:name])
  		end

  	end

		newest_criteria
  end
	
end