class PapersController < ApplicationController
	before_action :set_paper, only: [:edit, :destroy, :update, :apply_evaluator, :ratings, :evaluate, :apply_evaluation]

	def index
		@papers = Paper.all
	end

	def new
		@paper = Paper.new(event_id: params[:event_id])
	end

	def create
		@paper = Paper.new(paper_params)
		@paper.content = Paper.upload(params[:paper][:content]) if params[:paper][:content].present?

		students = create_students

		@paper.save
		@paper.students << students

		redirect_to papers_event_path(@paper.event)
	end

	def edit
	end

	def update
		@paper.update_attributes(paper_params)
		@paper.students << update_students

		redirect_to papers_event_path(@paper.event)
	end

	def destroy
		@paper.destroy

		redirect_to papers_path
	end

	def apply_evaluator
		@evaluator = User.find_by_id(params[:user_id])
		@paper.update_attributes(user: @evaluator) if @evaluator.user_type.teacher?

		redirect_to papers_event_path(@paper.event)
	end

	def ratings
		@ratings = @paper.ratings.includes(:criterium)
	end

	def evaluate
	end

	def apply_evaluation
		@paper.ratings.destroy_all

		params[:ratings].each do |rating| 
			Rating.create(criterium_id: rating[:id], value: rating[:value], user_id: params[:evaluator_id], paper: @paper, considerations: rating[:considerations])
		end

		redirect_to papers_users_path
	end


	private
	def set_paper
		@paper = Paper.find_by_id(params[:id])
	end

  def paper_params
    params.require(:paper).permit(:title, :description, :event_id, :user)
  end

  def create_students
  	students = []

  	params[:students].each do |student_param|
  		next if student_param[:remove]
  		students << Student.create(name: student_param[:name])
  	end

  	students
  end

  def update_students
		newest_students = []
  	params[:students].each do |student_param|

  		if !student_param[:id].blank? && student_param[:remove]
  			Student.find(student_param[:id]).destroy
  		elsif !student_param[:id].blank?
  			Student.find(student_param[:id]).update_attributes(name: student_param[:name])
  		elsif !student_param[:remove]
				newest_students << Student.create(name: student_param[:name])
  		end

  	end

		newest_students
  end
	
end