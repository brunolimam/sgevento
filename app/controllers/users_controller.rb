class UsersController < ApplicationController
	before_action :set_user, only: [:edit, :destroy, :update, :apply_evaluator, :ratings]

	def index
	end

	def new
		@user = User.new(event_id: params[:event_id])
	end

	def create
		@user = User.new(user_params)
		@user.save
	end

	def edit
	end

	def update
		@user.update_attributes(user_params)
	end

	def destroy
		@user.destroy
	end

	def papers
		@papers = current_user.papers.includes(:students, :event).order(title: :asc).paginate(:page => params[:page], :per_page => 10)
	end

	private
	def set_user
		@user = User.find_by_id(params[:id])
	end

  def user_params
    params.require(:user).permit(:name, :email, :password, :user_type_id)
  end

end