class AppController < ApplicationController
  def index
  	redirect_to events_path if current_user.user_type.owner?
  	redirect_to papers_users_path if current_user.user_type.teacher?
  end
end
