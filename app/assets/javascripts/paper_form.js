var removeStudentFields = function(id){
  $("#fieldset-quantity-student-" + id).hide();
  document.getElementById("student_remove_checkbox_" + id).checked = true;
}

var quantityStudents = 0

var addStudent = function() {
  var fields = '<div id="fieldset-quantity-student-' + quantityStudents + '">\
                    <fieldset class="form-inline">\
                      <input id="student_id_' + quantityStudents + '" type="text" hidden="true" name=students[][id]>\
                      \
                      <div class="row">\
                        <div class="col-lg-10">\
                          <input type="text" class="form-control form-control-sm" name=students[][name] placeholder="Nome" data-id-element="#student_id_' + quantityStudents + '" >\
                        </div>\
                      \
                        <div class="col-lg-2">\
                          <button id="quantity-students-' + quantityStudents + '" type="button" onclick="removeStudentFields(' + quantityStudents + ')" class="btn btn-info btn-sm remove-student">\
                            Remover\
                          </button>\
                      \
                          <input type="checkbox" id="student_remove_checkbox_' + quantityStudents + '" class="form-control form-control-sm" name=students[][remove] hidden>\
                        </div>\
                      </div>\
                    </fieldset>\
                    <br>\
                  </div>';


  $("#fields-students").append(fields);
  quantityStudents++;
}