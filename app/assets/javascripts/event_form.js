var removeCriteriumFields = function(id){
  $("#fieldset-quantity-criterium-" + id).hide();
  document.getElementById("criterium_remove_checkbox_" + id).checked = true;
}

var quantityCriteria = 0

var addCriterium = function() {
  var fields = '<div id="fieldset-quantity-criterium-' + quantityCriteria + '">\
                    <fieldset class="form-inline">\
                      <input id="criterium_id_' + quantityCriteria + '" type="text" hidden="true" name=criteria[][id]>\
                      \
                      <div class="row">\
                        <div class="col-lg-10">\
                          <input type="text" class="form-control form-control-sm" name=criteria[][name] placeholder="Critério" data-id-element="#criterium_id_' + quantityCriteria + '" >\
                        </div>\
                      \
                        <div class="col-lg-2">\
                          <button id="quantity-criteria-' + quantityCriteria + '" type="button" onclick="removeCriteriumFields(' + quantityCriteria + ')" class="btn btn-info btn-sm remove-criterium">\
                            Remover\
                          </button>\
                      \
                          <input type="checkbox" id="criterium_remove_checkbox_' + quantityCriteria + '" class="form-control form-control-sm" name=criteria[][remove] hidden>\
                        </div>\
                      </div>\
                    </fieldset>\
                    <br>\
                  </div>';


  $("#fields-criteria").append(fields);
  quantityCriteria++;
}