class Criterium < ApplicationRecord
  belongs_to :event
  has_many :ratings
end
