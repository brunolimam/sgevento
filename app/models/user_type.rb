class UserType < ApplicationRecord
	has_many :users

  enum kind: {owner: 'owner', teacher: 'teacher'}

  def pt_kind
  	owner? ? "Instituição" : "Professor"
  end
end
