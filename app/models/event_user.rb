class EventUser < ApplicationRecord
  belongs_to :event
  belongs_to :user

  enum status: {pendent: 0, accepted: 1, refused: 2}
end
