class Paper < ApplicationRecord
  belongs_to :event
	has_and_belongs_to_many :students
  has_many :ratings
  belongs_to :user, optional: true

	def self.upload file
		cloudnary_file = Cloudinary::Uploader.upload(file)
    cloudnary_file["secure_url"].present? ? cloudnary_file["secure_url"] : ""
	end

	def students_name_formatted
		students.map(&:name).map{ |name| name.partition(" ").first }.join(", ")
	end


	def has_rating_consideration?
		ratings.map(&:considerations).each {|consideration| return true if !consideration.blank?}

		false
	end
end
