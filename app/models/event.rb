class Event < ApplicationRecord
  belongs_to :user
 	
 	has_many :papers
  has_many :event_users
  has_many :teachers_event_users, -> { joins(:user).where(users: {user_type_id: UserType.teacher.first.id}) }, class_name: 'EventUser'

  has_one  :token

  has_many :criteria

  has_many :students, -> { where(user_type_id: UserType.student.first.id) }, through: :event_users, source: :user
  has_many :teachers, -> { where(user_type_id: UserType.teacher.first.id) }, through: :event_users, source: :user

  after_create :generate_token

  def valid_token? token
    self.token.key == token && self.token.active?
  end

  def bind_dates start_at, end_at, accept_until
    self.start_at             = DateTime.strptime(start_at, '%d/%m/%Y') if !start_at.blank?
    self.end_at               = DateTime.strptime(end_at, '%d/%m/%Y') if !start_at.blank?
    self.accept_papers_until  = DateTime.strptime(accept_papers_until, '%d/%m/%Y') if !start_at.blank?
  end

  private
  def generate_token
  	key = SecureRandom.base64(10).tr('+/=lIO0', 'pqrsxyz')
  	Token.create(key: key, event: self)
  end



end
