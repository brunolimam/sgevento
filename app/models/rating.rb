class Rating < ApplicationRecord
  belongs_to :paper
  belongs_to :user
  belongs_to :criterium
end
