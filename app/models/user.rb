class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  belongs_to :user_type
  has_many :papers
  has_many :ratings
  has_many :events

	has_many :event_users
  has_many :teacher_events, through: :event_users, source: :event  
end
