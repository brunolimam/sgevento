# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171123021907) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "criteria", force: :cascade do |t|
    t.string "name"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_criteria_on_event_id"
  end

  create_table "event_users", id: :serial, force: :cascade do |t|
    t.integer "event_id"
    t.integer "user_id"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_event_users_on_event_id"
    t.index ["user_id"], name: "index_event_users_on_user_id"
  end

  create_table "events", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.datetime "start_at"
    t.datetime "end_at"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "place"
    t.datetime "accept_papers_until"
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "papers", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "event_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content"
    t.index ["event_id"], name: "index_papers_on_event_id"
    t.index ["user_id"], name: "index_papers_on_user_id"
  end

  create_table "papers_students", force: :cascade do |t|
    t.bigint "paper_id"
    t.bigint "student_id"
    t.index ["paper_id"], name: "index_papers_students_on_paper_id"
    t.index ["student_id"], name: "index_papers_students_on_student_id"
  end

  create_table "ratings", id: :serial, force: :cascade do |t|
    t.float "value"
    t.text "considerations"
    t.integer "paper_id"
    t.integer "user_id"
    t.integer "criterium_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["criterium_id"], name: "index_ratings_on_criterium_id"
    t.index ["paper_id"], name: "index_ratings_on_paper_id"
    t.index ["user_id"], name: "index_ratings_on_user_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.bigint "paper_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["paper_id"], name: "index_students_on_paper_id"
  end

  create_table "tokens", force: :cascade do |t|
    t.string "key"
    t.boolean "active", default: true
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_tokens_on_event_id"
  end

  create_table "user_types", id: :serial, force: :cascade do |t|
    t.string "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.integer "user_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["user_type_id"], name: "index_users_on_user_type_id"
  end

  add_foreign_key "criteria", "events"
  add_foreign_key "event_users", "events"
  add_foreign_key "event_users", "users"
  add_foreign_key "events", "users"
  add_foreign_key "papers", "events"
  add_foreign_key "papers_students", "papers"
  add_foreign_key "papers_students", "students"
  add_foreign_key "ratings", "criteria"
  add_foreign_key "ratings", "papers"
  add_foreign_key "ratings", "users"
  add_foreign_key "students", "papers"
  add_foreign_key "users", "user_types"
end
