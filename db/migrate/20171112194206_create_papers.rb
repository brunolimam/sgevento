class CreatePapers < ActiveRecord::Migration[5.0]
  def change
    create_table :papers do |t|
      t.string :title
      t.text :description
      t.references :event, foreign_key: true
      t.references :user

      t.timestamps
    end
  end
end
