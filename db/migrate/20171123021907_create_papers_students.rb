class CreatePapersStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :papers_students do |t|
      t.references :paper, foreign_key: true
      t.references :student, foreign_key: true
    end
  end
end
