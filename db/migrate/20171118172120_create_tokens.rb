class CreateTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :tokens do |t|
      t.string :key
      t.boolean :active, default: true
      t.references :event

      t.timestamps
    end
  end
end
