class AddAcceptPapersUntilToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :accept_papers_until, :timestamp
  end
end
