class CreateRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :ratings do |t|
      t.float :value
      t.text :considerations
      t.references :paper, foreign_key: true
      t.references :user, foreign_key: true
      t.references :criterium, foreign_key: true

      t.timestamps
    end
  end
end
