class AddContentToPapers < ActiveRecord::Migration[5.1]
  def change
    add_column :papers, :content, :string
  end
end
