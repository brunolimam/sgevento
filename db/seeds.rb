UserType.create([{kind: "owner"},
								{kind: "teacher"}])

letters = ["A", "B", "C"]

first_user 	= User.create(user_type: UserType.first, email: "brunolima@gmail.com", password: "12341234", name: "Bruno Lima")

teachers = []
10.times do |i| 
	teachers << User.create(user_type: UserType.second, email: "kayo#{i+1}@gmail.com", password: "12341234", name: "Kayo #{i+1}")
end

students = []
students << Student.create(name: "Willian Brandão")
students << Student.create(name: "Wendell Paiva")

events = []
15.times do |i|
	events << Event.create(title: "Evento #{letters[i%3]}", place: "Pátio IFCE", user: first_user, start_at: DateTime.now+(2+i).days)
end

criteria = []
4.times do |i|
	criteria << Criterium.create(name: "Critério #{letters[i%3]}", event: events.first)
end

events.first.teachers << teachers

papers = []
teacher = User.second
15.times do |i|
	paper = Paper.create(event: events.first, user: teacher, title: "Trabalho de cálculo #{letters[i%3]}", description: "Esse trabalho eu tenho que fazer")
	paper.students << students

	papers << paper
end

criteria.each do |criterium|
	Rating.create(criterium: criterium, value: 7.5, user: teachers.first, paper: papers.first, considerations: "Essa é uma nota de teste")
end
